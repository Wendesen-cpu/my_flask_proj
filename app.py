from flask import request, Flask
import json
appp = Flask(__name__)
@appp.route('/plus_one')
def plus_one():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 1})
@appp.route('/square')
def square():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x * x})